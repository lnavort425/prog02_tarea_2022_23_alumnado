/*
 * Ejercicio 02
 */
package tarea02;


/**
 *
 * @author david
 */
public class Ejercicio02 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        //----------------------------------------------
        //    Declaración de variables y constantes
        //----------------------------------------------
        // Variables de entrada (aquí se definen las variables que recibirán valores, si fueran necesarias)
        // Variables de salida (aquí se definen las variables que almacenarán resultados y se mostrarán al usuario, si fueran necesarias)
        // Clase Scanner para petición de datos al usuario a través del teclado
        //----------------------------------------------
        //               Entrada de datos 
        //----------------------------------------------
        System.out.println("Ejercicio 2. Signos del Zodíaco");
        System.out.println("----------------------------------------------------");



        //----------------------------------------------
        //                 Procesamiento 
        //----------------------------------------------
       //1. pido dia 
       // pido mes
       // si mes no (correcto >=1 y <=12 ) o dia no  es (correcto >=1 y <=31)
       //   resultado= fecha incorrecto
       // sino
       //   switch(mes)
       //       4,6,9,11: correcto <=30
       //       2: correcto si <=29
       //       default: si <=31
       // si fecha correcta
       //   switch(mes)
       //       1: si dia<21 resultado="capricornio"
       //            sino resultado="acuario";
       //           break;
       //       2: si dia<21 resultado="acario"
       //            sino resultado="piscis";
       //           break;
        //----------------------------------------------
        //              Salida de resultados 
        //----------------------------------------------
           

    }

}
