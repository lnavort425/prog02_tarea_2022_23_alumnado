package tarea02;

import java.util.Random;
import java.util.Scanner;

/**
 * Ejercicio 4: Histograma
 *
 * La aplicación para simular la generación de un histograma por consola. Esto
 * se consigue mediante la generación automática de una especie de gráfico con
 * diferentes símbolos. Para ello se escribe un programa en Java que solicite al
 * usuario un número filas que deseemos que tenga el programa. Este número
 * deberá ser mayor o igual que 12 (el programa no debe avanzar si se introduce
 * un número inferior a 12, seguirá pidiendo un número igual o superior a 12
 * hasta que introduzca un valor válido). Posteriormente, se ajustará dicho
 * número de filas introducido para que sea el mayor número múltiplo de 4. Es
 * decir, si se obtiene un número que no es múltiplo de cuatro, se debe ajustar
 * al múltiplo menor más próximo.
 *
 * @author Programación DAM y DAW IES Trassierra
 */
public class Ejercicio04 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        // Variable para almacenar el objeto Scanner para gestionar la entrada de datos por teclado
        Scanner teclado = new Scanner(System.in);

        // Variable para almacenar el objeto Random para gestionar la generación de números aleatorios
        Random r = new Random();

        // Constante para asignar el número máximo de símbolos (aun sin asignar valor)
        final int MAX_SIMBOLOS;

        //Variable para almacenar el número de filas del histograma.
        int filas;

        String cadena = "--------------------------------------------\n";
        //----------------------------------------------
        //               Entrada de datos
        //----------------------------------------------
        System.out.println("Ejercicio 4. Histograma");
        System.out.println("----------------------------------------------------");

        /*
            Se realiza la validación de los datos de entrada, al no saber el número de
            veces que se va a repetir el bucle NO se utiliza el for, y al ser mínimo una
            vez se utiliza do-while
         */
        do {
            System.out.println("Introduce el número de filas del histograma (Mayor que 10)");
            filas = teclado.nextInt();
        } while (filas < 12);

        /*
            Se realiza la simplificación a un múltiplo de cuatro.
            Para ello, al número de filas se le quita el resto de dividir entre cuatro
         */
        filas -= filas % 4;

        /*
            Se calcula el número máximo de simbolos que puede tener una fila. Este
            valor será la mitad del número de filas máximo que tendrá finalmente
            el histograma, una vez ajustado a múltiplo de 4.
         */
        MAX_SIMBOLOS = filas / 2;

        // Se realiza un primer for para ir generando cada fila del histograma
        for (int i = 0; i < filas; i++) {

            /*
                En cada iteración se presentará un símbolo diferente. Al se cuatro
                símbolos, estos se repetirán cada cuatro filas, por tanto se toma
                el resto de la división entera de las filas entre cuatro. Se
                obtiene que en cada valor de índice de fila, se tiene un símbolo
                diferente, repitiéndose de cuatro en cuatro.
             */
            String simbolo = "";
            switch (i % 4) {
                case 0:
                    simbolo = "*";
                    break;
                case 1:
                    simbolo = "#";
                    break;
                case 2:
                    simbolo = "@";
                    break;
                case 3:
                    simbolo = ">";
                    break;
            } // Fin switch selección simbolo.

            /*
                Con Random se genera un número de símbolos entre 3 y el número
                máximo de símbolos. Al tener un mínimo de 3, este se debe
                restar al número máximo de simbolos al generar el aleatorio.
                De esta forma se obtiene un número entre 3 y el número máximo de
                símbolos.
             */
            int numSimbolos = 3 + r.nextInt(MAX_SIMBOLOS - 3);

           
            /*
                 Para cada fila se generan tantos símbolos como nos indica el número
                 generado aleatoriamente para cada fila. Para ello se concatena
                 tantas veces como dicho aleatorio indique, y con el símbolo
                 seleccionado anteriormente en el switch
             */
            for (int j = 0; j < numSimbolos; j++) {
                cadena += simbolo;
            }

            // Cada cuatro líneas, se presenta una línea horizontal con guiones
            if (i % 4 == 3) {
                cadena += "\n--------------------------------------------\n";
            } else {
                cadena += "\n";
            }

        } //Fin for filas

        //----------------------------------------------
        //              Salida de resultados
        //----------------------------------------------
        System.out.println();
        System.out.println("RESULTADO");
        System.out.println("---------");
        System.out.println("El histograma va a presentar " + filas + " filas.");
        System.out.print(cadena);

    } // Fin main
} // Fin class

